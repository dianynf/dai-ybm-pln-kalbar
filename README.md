Bismillah. Cara Instalasi Proyek Dai YBM PLN Kalimantan Barat

1. clone terlebih dahulu reponya : https://gitlab.com/dianynf/dai-ybm-pln-kalbar.git
2. masuk ke repo : cd dai-ybm-pln-kalbar
3. jalankan perintah : composer install
4. rename file " .env.example " menjadi file " .env "
5. untuk install database bisa dua cara
    a. import file database " dai-ybm-pln-kalbar ". di dalam direktori database
    b. bisa jalankan php artisan migrate

    Selesai 
