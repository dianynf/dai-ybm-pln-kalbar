<?php

namespace App\Exports;

use App\Kegiatan;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;


class KegiatansExport implements FromCollection, WithHeadings, WithMapping, WithEvents
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Kegiatan::select('users_id', 'tgl', 'jml_kajian', 'jml_jamaah', 'deskripsi', 'alamat')->get();
    }

    public function map($kegiatan): array
    {
        return [
            $kegiatan->user->username,
            $kegiatan->tgl,
            $kegiatan->jml_kajian,
            $kegiatan->jml_jamaah,
            $kegiatan->deskripsi,
            $kegiatan->alamat,
        ];
    }

    public function registerEvents(): array
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];

        return [
            AfterSheet::class    => function (AfterSheet $event) use ($styleArray) {
                $event->sheet->getStyle('A1:W1')->applyFromArray($styleArray); // All headers
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(20);
            },
        ];
    }

    public function headings(): array
    {
        return [
            'Nama Ustadz',
            'Tanggal',
            'Jumlah Kajian',
            'Jumlah Jamaah',
            'Deskripsi',
            'Alamat',
        ];
    }
}
