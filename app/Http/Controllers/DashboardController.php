<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kegiatan;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user_id = Auth::user()->role_id;
        $total_mualaf = DB::table("mualafs")->sum('jml_mualaf');
        $total_kegiatan = DB::table("kegiatans")->count();
        $total_kab = DB::table("kegiatans")->distinct()->count('id_kab');
        $total_kec = DB::table("kegiatans")->distinct()->count('id_kec');
        $total_desa = DB::table("kegiatans")->distinct()->count('id_desa');
        $janu = DB::table("kegiatans")->where('month', '=', '01')->count();
        $feb = DB::table("kegiatans")->where('month', '=', '02')->count();
        $mar = DB::table("kegiatans")->where('month', '=', '03')->count();
        $apr = DB::table("kegiatans")->where('month', '=', '04')->count();
        $mei = DB::table("kegiatans")->where('month', '=', '05')->count();
        $jan = DB::table("kegiatans")->where('month', '=', '06')->count();
        $jul = DB::table("kegiatans")->where('month', '=', '07')->count();
        $ags = DB::table("kegiatans")->where('month', '=', '08')->count();
        $sep = DB::table("kegiatans")->where('month', '=', '09')->count();
        $okt = DB::table("kegiatans")->where('month', '=', '10')->count();
        $nov = DB::table("kegiatans")->where('month', '=', '11')->count();
        $des = DB::table("kegiatans")->where('month', '=', '12')->count();

        $janum = DB::table("mualafs")->where('month', '=', '01')->count();
        $febm = DB::table("mualafs")->where('month', '=', '02')->count();
        $marm = DB::table("mualafs")->where('month', '=', '03')->count();
        $aprm = DB::table("mualafs")->where('month', '=', '04')->count();
        $meim = DB::table("mualafs")->where('month', '=', '05')->count();
        $janm = DB::table("mualafs")->where('month', '=', '06')->count();
        $julm = DB::table("mualafs")->where('month', '=', '07')->count();
        $agsm = DB::table("mualafs")->where('month', '=', '08')->count();
        $sepm = DB::table("mualafs")->where('month', '=', '09')->count();
        $oktm = DB::table("mualafs")->where('month', '=', '10')->count();
        $novm = DB::table("mualafs")->where('month', '=', '11')->count();
        $desm = DB::table("mualafs")->where('month', '=', '12')->count();

        $total_dai =
            DB::table('users')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('users.role_id', '=', 2)
            ->get()->count('role_id');

        return view('pages.dashboard.index', [
            'total_mualaf' => $total_mualaf,
            'total_kegiatan' => $total_kegiatan,
            'total_dai' => $total_dai,
            'total_kab' => $total_kab,
            'total_kec' => $total_kec,
            'total_desa' => $total_desa,
            'janu' => $janu,
            'feb' => $feb,
            'mar' => $mar,
            'apr' => $apr,
            'mei' => $mei,
            'jan' => $jan,
            'jul' => $jul,
            'ags' => $ags,
            'sep' => $sep,
            'okt' => $okt,
            'nov' => $nov,
            'des' => $des,

            'janum' => $janum,
            'febm' => $febm,
            'marm' => $marm,
            'aprm' => $aprm,
            'meim' => $meim,
            'janm' => $janm,
            'julm' => $julm,
            'agsm' => $agsm,
            'sepm' => $sepm,
            'oktm' => $oktm,
            'novm' => $novm,
            'desm' => $desm,
        ]);
    }
}
