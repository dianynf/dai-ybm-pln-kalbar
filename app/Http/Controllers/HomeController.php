<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_mualaf = DB::table("mualafs")->sum('jml_mualaf');
        $total_kegiatan = DB::table("kegiatans")->count('id');
        $total_dai =
            DB::table('users')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('users.role_id', '=', 2)
            ->get()->count('role_id');
        return view('pages.home', [
            'total_mualaf' => $total_mualaf,
            'total_dai' => $total_dai,
            'total_kegiatan' => $total_kegiatan,
        ]);
    }
}
