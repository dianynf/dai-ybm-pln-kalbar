<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use App\KegiatanDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Reg_districts;
use App\Reg_regencies;
use App\Reg_villages;
use App\User;
use Illuminate\Support\Facades\DB;

use App\Exports\KegiatansExport;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpKernel\Kernel;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inputss(Request $request)
    {
        return view('pages.kegiatan.index');
    }

    public function create()
    {
        //
    }

    public function input()
    {
        $departmentData['data'] = Reg_regencies::orderby("name", "asc")
            ->select('id', 'name')
            ->get();

        return view('pages.kegiatan.index')->with("departmentData", $departmentData);
    }

    public function getDistricts($departmentid = 0)
    {
        $empData['data'] = Reg_districts::orderby("name", "asc")
            ->select('id', 'name')
            ->where('regency_id', $departmentid)
            ->get();

        return response()->json($empData);
    }

    public function getVillages($distid = 0)
    {
        $distData['data'] = Reg_villages::orderby("name", "asc")
            ->select('id', 'name')
            ->where('district_id', $distid)
            ->get();

        return response()->json($distData);
    }

    public function store(Request $request)
    {
        $expiry = explode("/", $request->tgl);

        $date = $expiry[0];
        $month = $expiry[1];
        $year = $expiry[2];

        // dd($date, $month, $year);
        $lastid = Kegiatan::create([
            'users_id' => Auth::user()->id,
            'tgl' => $request->tgl,
            'jml_kajian' => $request->jml_kajian,
            'jml_jamaah' => $request->jml_jamaah,
            'deskripsi' => $request->deskripsi,
            'alamat' => $request->alamat,
            'id_kab' => $request->id_kab,
            'id_kec' => $request->id_kec,
            'id_desa' => $request->id_desa,
            'date' => $date,
            'month' => $month,
            'year' => $year
        ])->id;

        $files = [];
        foreach ($request->file('foto') as $file) {
            $files[] = [
                'kegiatans_id' => $lastid,
                'foto' => $file->getClientOriginalName()
            ];
            $file->storeAs('public/dokumentasi', $file->getClientOriginalName());
        }
        KegiatanDetail::insert($files);

        session()->flash('sukses', 'Data berhasil di Input');
        return redirect('/kegiatan');
    }

    public function index()
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {

            $items = Kegiatan::with([
                'user', 'desa', 'kab', 'kec'
            ])->get();

            return view('pages.cms.kegiatan.index', [
                'items' => $items
            ]);
        } elseif ($user_id == 2) {
            $items = Kegiatan::with([
                'user', 'desa', 'kab', 'kec'
            ])->where('users_id', '=', Auth::user()->id)
                ->get();
            return view('pages.cms.kegiatan.index', [
                'items' => $items
            ]);
        }
    }

    public function show($id)
    {
        $item = Kegiatan::with([
            'fotos', 'user', 'kab', 'kec', 'desa'
        ])->findOrFail($id);
        // dd($item->fotos);
        return view('pages.cms.kegiatan.detail', [
            'item' => $item
        ]);
    }

    public function edit(Kegiatan $kegiatan)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $user = User::all();
            $fotos = KegiatanDetail::all();
            $kab = Reg_regencies::all();
            $kec = Reg_districts::all();
            $desa = Reg_villages::all();
            return view('pages.cms.kegiatan.edit', [
                'kegiatan' => $kegiatan,
                'fotos' => $fotos,
                'kab' => $kab,
                'desa' => $desa,
                'kec' => $kec,
                'user' => $user
            ]);
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Kegiatan $kegiatan)
    {
        $this->validate($request, [
            'tgl' => 'required',
            'jml_kajian' => 'required|numeric',
            'jml_jamaah' => 'required|numeric',
            'alamat' => 'required',
            'deskripsi' => 'required',
            'id_kab' => 'required',
            'id_kec' => 'required',
            'id_desa' => 'required',
        ]);

        $kegiatan->tgl = $request->tgl;
        $kegiatan->jml_kajian = $request->jml_kajian;
        $kegiatan->jml_jamaah = $request->jml_jamaah;
        $kegiatan->deskripsi = $request->deskripsi;
        $kegiatan->alamat = $request->alamat;
        $kegiatan->id_kab = $request->id_kab;
        $kegiatan->id_kec = $request->id_kec;
        $kegiatan->id_desa = $request->id_desa;
        $kegiatan->save();

        if ($kegiatan->save()) {
            $request->session()->flash('sukses', 'Data berhasil di Ubah');
        } else {
            $request->session()->flash('error', 'has been error updated');
        }
        session()->flash('sukses', 'Data kegiatan berhasil edit');
        return redirect(route('kegiatans.index'));
    }

    public function destroy(Kegiatan $kegiatan)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $kegiatan->delete();
            session()->flash('sukses', 'Data berhasil dihapus');
            return redirect()->route('kegiatans.index');
        } else {
            return abort(404);
        }
    }


    public function export_excel()
    {
        return Excel::download(new KegiatansExport, 'Kegiatan Dai 2021.xlsx');
    }
}
