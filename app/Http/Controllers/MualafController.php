<?php

namespace App\Http\Controllers;

use App\Mualaf;
use App\MualafDetails;
use App\Http\Controllers\Controller;
use App\MualafDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Reg_regencies;
use App\Reg_districts;
use App\Reg_villages;
use App\User;
use Illuminate\Support\Facades\DB;

use App\Exports\MualafsExport;
use Maatwebsite\Excel\Facades\Excel;


class MualafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // bagian LMS
    public function input(Request $request)
    {
        $departmentData['data'] = Reg_regencies::orderby("name", "asc")
            ->select('id', 'name')
            ->get();

        return view('pages.mualaf.index')->with("departmentData", $departmentData);
    }

    public function getDistricts($departmentid = 0)
    {

        // Fetch Employees by Departmentid
        $empData['data'] = Reg_districts::orderby("name", "asc")
            ->select('id', 'name')
            ->where('regency_id', $departmentid)
            ->get();

        return response()->json($empData);
    }

    public function getVillages($distid = 0)
    {

        // Fetch Employees by distid
        $distData['data'] = Reg_villages::orderby("name", "asc")
            ->select('id', 'name')
            ->where('district_id', $distid)
            ->get();

        return response()->json($distData);
    }

    public function create(Request $request)
    {
        $expiry = explode("/", $request->tgl);

        $date = $expiry[0];
        $month = $expiry[1];
        $year = $expiry[2];

        $lastid = Mualaf::create([
            'users_id' => Auth::user()->id,
            'jml_mualaf' => $request->jml_mualaf,
            'deskripsi' => $request->deskripsi,
            // 'alamat' => $request->alamat,
            // 'id_kab' => $request->id_kab,
            // 'id_kec' => $request->id_kec,
            // 'id_desa' => $request->id_desa,
            'tgl' => $request->tgl,
            'date' => $date,
            'month' => $month,
            'year' => $year
        ])->id;

        $this->validate($request, [
            'foto' => 'required',
            'foto.*' => 'mimes:jpeg,jpg,png|max:2000'
        ]);

        $files = [];
        foreach ($request->file('foto') as $file) {
            $path = $file->store('public/ktp');
            $files[] = [
                'mualafs_id' => $lastid,
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'foto' => $file->getClientOriginalName()
            ];
        }
        MualafDetail::insert($files);

        session()->flash('sukses', 'Data berhasil di Input');
        return redirect('/mualaf');
    }

    // bagian CMS
    public function index()
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $items = Mualaf::with([
                'user'
            ])->get();
            return view('pages.cms.mualaf.index', [
                'items' => $items
            ]);
        } elseif ($user_id == 2) {
            $items = Mualaf::with([
                'user'
            ])->where('users_id', '=', Auth::user()->id)
                ->get();
            return view('pages.cms.mualaf.index', [
                'items' => $items
            ]);
        }
    }

    public function show(Mualaf $mualaf)
    {
        $user = User::all();
        // $kab = Reg_regencies::all();
        // $kec = Reg_districts::all();
        // $desa = Reg_villages::all();
        $mualafs = MualafDetail::all();

        return view('pages.cms.mualaf.detail', [
            'mualaf' => $mualaf,
            'mualafs' => $mualafs,
            // 'kab' => $kab,
            // 'desa' => $desa,
            // 'kec' => $kec,
            'user' => $user
        ]);
    }

    public function edit(Mualaf $mualaf)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $user = User::all();
            $kab = Reg_regencies::all();
            $kec = Reg_districts::all();
            $desa = Reg_villages::all();
            return view('pages.cms.mualaf.edit', [
                'mualaf' => $mualaf,
                // 'kab' => $kab,
                // 'desa' => $desa,
                // 'kec' => $kec,
                'user' => $user
            ]);
        } else {
            return abort(404);
        }
    }

    public function update(Request $request, Mualaf $mualaf)
    {
        $this->validate($request, [
            'tgl' => 'required',
            'jml_mualaf' => 'required|numeric',
            'deskripsi' => 'required',
            // 'alamat' => 'required',
            // 'id_kab' => 'required',
            // 'id_kec' => 'required',
            // 'id_desa' => 'required',
        ]);

        $mualaf->tgl = $request->tgl;
        $mualaf->jml_mualaf = $request->jml_mualaf;
        $mualaf->deskripsi = $request->deskripsi;
        // $mualaf->alamat = $request->alamat;
        // $mualaf->id_kab = $request->id_kab;
        // $mualaf->id_kec = $request->id_kec;
        // $mualaf->id_desa = $request->id_desa;
        $mualaf->save();

        if ($mualaf->save()) {
            $request->session()->flash('sukses', 'Data berhasil di Ubah');
        } else {
            $request->session()->flash('error', 'has been error updated');
        }
        session()->flash('sukses', 'Data Mualaf berhasil edit');
        return redirect(route('mualafs.index'));
    }

    public function destroy(Mualaf $mualaf)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $mualaf->delete();
            session()->flash('sukses', 'Data berhasil dihapus');
            return redirect()->route('mualafs.index');
        } else {
            return abort(404);
        }
    }

    public function export_excel()
    {
        return Excel::download(new MualafsExport, 'Data Mualaf 2021.xlsx');
    }
}
