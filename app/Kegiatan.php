<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $fillable = [
        'jml_jamaah', 'jml_kajian', 'alamat', 'deskripsi', 'id_kab', 'id_kec', 'id_desa', 'tgl', 'users_id', 'date', 'month', 'year'
    ];

    // protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function kab()
    {
        return $this->belongsTo(Reg_regencies::class, 'id_kab', 'id');
    }

    public function kec()
    {
        return $this->belongsTo(Reg_districts::class, 'id_kec', 'id');
    }

    public function desa()
    {
        return $this->belongsTo(Reg_villages::class, 'id_desa', 'id');
    }

    public function fotos()
    {
        return $this->hasMany(KegiatanDetail::class, 'kegiatans_id', 'id');
    }
}
