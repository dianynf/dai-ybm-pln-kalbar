<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mualaf extends Model
{
    // use SoftDeletes;

    protected $fillable = [
        'jml_mualaf', 'deskripsi',  'tgl', 'users_id', 'date', 'month', 'year'
    ];

    // 'alamat', 'id_kab', 'id_kec', 'id_desa',

    // protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    // public function kab()
    // {
    //     return $this->belongsTo(Reg_regencies::class, 'id_kab', 'id');
    // }

    // public function kec()
    // {
    //     return $this->belongsTo(Reg_districts::class, 'id_kec', 'id');
    // }

    // public function desa()
    // {
    //     return $this->belongsTo(Reg_villages::class, 'id_desa', 'id');
    // }

    public function fotos()
    {
        return $this->hasMany(MualafDetail::class, 'mualafs_id', 'id');
    }
}
