<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reg_districts extends Model
{
    protected $fillable = [
        'regency_id', 'name'
    ];

    public function mualafs()
    {
        return $this->hasMany(Mualaf::class, 'id_kec', 'id');
    }

    public function kegiatans()
    {
        return $this->hasMany(Kegiatan::class, 'id_kec', 'id');
    }
}
