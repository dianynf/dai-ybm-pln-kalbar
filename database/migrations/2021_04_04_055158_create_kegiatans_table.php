<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kegiatans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tgl');
            $table->string('jml_kajian');
            $table->string('jml_jamaah');
            $table->text('deskripsi')->nullable();
            $table->string('alamat')->nullable();
            $table->bigInteger('id_kab');
            $table->bigInteger('id_kec');
            $table->bigInteger('id_desa');
            $table->bigInteger('users_id');
            $table->bigInteger('year');
            $table->bigInteger('date');
            $table->string('month')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kegiatans');
    }
}
