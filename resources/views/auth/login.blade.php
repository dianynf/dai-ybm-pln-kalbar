@extends('layouts.auth')
@section('title', "Login - Da'i YBM PLN KAL-BAR")
@section('content')
    <main id="main">
        <section id="event-login" class="event-login mt-5">
            <div class="container">
                <div class="card">
                    <div class="col-md-8 offset-md-2">
                        <div class="card-body shadow p-3 mb-5 bg-white rounded">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <h3 class="col-md-12 mb-4 text-center">Login User</h3>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required
                                        autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" placeholder="Password"
                                        name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <button type="submit"
                                    class="btn btn-learn-btn col-md-4 offset-md-4 mt-3 mb-3">{{ __('Login') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
