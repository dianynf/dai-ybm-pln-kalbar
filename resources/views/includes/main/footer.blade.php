<footer id="footer" class="footer mt-5">
    <div class="footer-top">
        <div class="container">
            @if (session()->has('kontak'))
                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                    {{ session()->get('kontak') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row gy-4">
                <div class="col-lg-5 col-md-12 footer-info">
                    <a href="#" class="logo d-flex align-items-center">
                        <img class="img-fluid" src="{{ asset('fronten/assets/img/logo/logo-footer.png') }}"
                            alt="YBM PLN" style="max-height: 80px;">
                    </a>
                    <h3 class="mt-5">Yayasan Baitul Maal PLN</h3>
                    <p class="mt-2"><i class="icofont-google-map"></i> Alamat: Jl. Adi Sucipto No.4, Sungai Raya,
                        Kec. Sungai Raya, <br> Kabupaten Kubu Raya, Kalimantan Barat 78234</p>
                    <p class="mt-4"><i class="icofont-envelope"></i> Email: info@example.com</p>
                    <p class="mt-1"><i class="icofont-phone"></i> Nomor Telpon: +6221312312312</p>
                </div>

                <div class="col-lg-3 col-6 footer-links">
                    <h4>OPERASIONAL KANTOR</h4>
                    <ul>
                        <li><i class="fa fa-clock-o"></i> Senin - Jumat : 8.00 am - 16.00 pm</li>
                        <li><i class="fa fa-clock-o"></i> Sabtu - Ahad :<a class="bg-white btn-tutup">Tutup</a></li>
                    </ul>
                    <h4 class="mt-5">Ikuti Kami</h4>
                    <div class="social-links mt-2">
                        <a href="https://www.twitter.com/ybmpln/" class="twitter"><i class="icofont-twitter"></i></a>
                        <a href="https://www.facebook.com/ybmpln/" class="facebook"><i class="icofont-facebook"></i></a>
                        <a href="https://www.instagram.com/ybmpln/" class="instagram"><i
                                class="icofont-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UCD8sZAllQgkNvXf43sHS1tQ" class="youtube"><i
                                class="icofont-youtube"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12 footer-contact text-md-start">
                    <form method="POST" action="/kontak/store">
                        {{ csrf_field() }}
                        <h4>Kontak Kami</h4>
                        <div class="form-group">
                            <input type="email" required class="form-control" name="email" id="email"
                                placeholder="Email Address" data-rule="minlen:4"
                                data-msg="Please enter at least 8 chars of subject" />
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" required name="deskripsi" rows="5" data-rule="required"
                                data-msg="Please write something for us" placeholder="Pesan"></textarea>
                            <div class="validate"></div>
                        </div>
                        <div class="form-group">
                            <button class="btn-sm bg-white btn-footer" type="submit">Kirim</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>YBM PLN KAL-BAR</span></strong>
        </div>
    </div>
</footer>
