<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
        <div class="contact-info mr-auto">
            <i class="icofont-envelope"></i> <a href="mailto:contact@example.com">ybm@pln.co.id</a>
            <i class="icofont-phone"></i> 021-7261122 ext 1574
            <i class="icofont-clock-time"></i> Senin - Jum'at 8:00 s/d 16:00
        </div>
        <div class="social-links">
            <a href="https://www.twitter.com/ybmpln/" class="twitter"><i class="icofont-twitter"></i></a>
            <a href="https://www.facebook.com/ybmpln/" class="facebook"><i class="icofont-facebook"></i></a>
            <a href="https://www.instagram.com/ybmpln/" class="instagram"><i class="icofont-instagram"></i></a>
            <a href="https://www.youtube.com/channel/UCD8sZAllQgkNvXf43sHS1tQ" class="youtube"><i
                    class="icofont-youtube"></i></a>
        </div>
    </div>
</div>

<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
        <a href="{{ route('home') }}" class="logo mr-auto"><img src="{{ asset('fronten/assets/img/icons.png') }}"
                alt="YBM PLN" class="img-fluid"></a>

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('profiles') }}">Profile</a></li>
                <li><a href="{{ route('berita') }}">Berita</a></li>
                <li><a href="{{ route('gambar') }}">Gambar</a></li>
                <li><a href="{{ route('kontak') }}">Kontak</a></li>
                @guest
                    <li>
                        <a href="{{ route('login') }}" class="appointment-btn btn-primary text-center"
                            style="color: #fff; width: 90px; margin: 0px 2px;"><b>Login</b></a>
                    </li>
                @endguest
                @auth
                    <li class="drop-down"><a href="">Input Da'i</a>
                        <ul>
                            <li><a href="{{ route('kegiatan_input') }}">Data Kegiatan</a></li>
                            <li><a href="{{ route('mualaf') }}">Data Mualaf</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('dashboard') }}">Dashboard</span></a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#logoutModal"
                            class="appointment-btn btn-primary text-center"
                            style="color: #fff; width: 80px; margin: 0px 2px;">Keluar</span></a>
                    </li>
                @endauth
            </ul>
        </nav>
    </div>
</header>
