<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>@yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    @stack('prepend-style')
    @include('includes.auth.style')
    @stack('addon-style')
</head>

<body>
    @include('includes.auth.navbar')
    @yield('content')

    @include('includes.auth.footer')

    @stack('prepend-script')
    @include('includes.auth.script')
    @stack('addon-script')
</body>

</html>
