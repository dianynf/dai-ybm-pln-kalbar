<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>@yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    @include('includes.main.style')
</head>

<body>
    @include('includes.main.navbar')

    @yield('content')

    @include('includes.main.footer')

    @include('includes.main.script')
</body>

</html>
