@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card border-left-primary shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="h3 mb-0 text-gray-800">Tambah User</h5>
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('users.store') }}">
                    @csrf
                    <input type="text" hidden name="date" value="{{ date('Y-m-d\TH:i') }}">
                    <div class="form-group">
                        <input id="username" type="text" class="form-control" name="username"
                            value="{{ old('username') }}" required placeholder="Nama Lengkap">
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email"
                            placeholder="E-Mail Address">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <p> *Mohon isi email dengan benar, email yang sudah terdaftar tidak dapat
                            didaftarkan kembali</p>
                    </div>
                    <div class="form-group" id="only-number">
                        <input type="number" class="form-control input-number @error('nohp') is-invalid @enderror" required
                            maxlength="13" id="nohp" name="nohp" placeholder="Nomor Telepon" value="{{ old('nohp') }}">
                        @error('nohp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="show_hide_password">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="new-password" placeholder="Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="show_hide_passwordd">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                required autocomplete="new-password" placeholder="Konfirmasi Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col" style="text-align: right;">
                                @if (Route::has('password.request'))
                                    <a class="small link-lupp" style="text-decoration:none;"
                                        href="{{ route('password.request') }}">
                                        {{ __('Lupa Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">
                        Daftar
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
