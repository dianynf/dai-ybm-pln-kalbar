@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="card border-left-primary shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="h3 mb-0 text-gray-800">User {{ $user->username }}</h5>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Email</th>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Handphone</th>
                            <td>{{ $user->nohp }}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>{{ $user->alamat }}</td>
                        </tr>
                        <tr>
                            <th>Akun didaftarkan</th>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Role</th>
                            <td>
                                @if ($user->role_id == 1)
                                    Admin
                                @else
                                    Da'i
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Foto</th>
                            <td><img src="{{ Storage::url('public/fotoselfi/' . $user->image) }}" class="img-thumbnail"
                                    width="30%">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
