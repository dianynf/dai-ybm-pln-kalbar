@extends('layouts.admin')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">Edit Data</h3>
                                        <hr>
                                        <form method="POST" action="{{ route('kegiatans.update', $kegiatan->id) }}">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-group row">
                                                <label for="tgl" class="col-sm-2 col-form-label">Tanggal</label>
                                                <div class="col-sm-10">
                                                    <input id="tgl" type="text" class="form-control datepicker" name="tgl"
                                                        value="{{ old('tgl', $kegiatan->tgl) }}" autocomplete="tgl"
                                                        autofocus>
                                                    @error('tgl')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jml_kajian" class="col-sm-2 col-form-label">Jumlah
                                                    Kajian</label>
                                                <div class="col-sm-10">
                                                    <input id="jml_kajian" type="number"
                                                        class="form-control @error('jml_kajian') is-invalid @enderror"
                                                        name="jml_kajian"
                                                        value="{{ old('jml_kajian', $kegiatan->jml_kajian) }}"
                                                        autocomplete="jml_kajian" autofocus>
                                                    @error('jml_kajian')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jml_jamaah" class="col-sm-2 col-form-label">Jumlah
                                                    Jamaah</label>
                                                <div class="col-sm-10">
                                                    <input id="jml_jamaah" type="number"
                                                        class="form-control @error('jml_jamaah') is-invalid @enderror"
                                                        name="jml_jamaah"
                                                        value="{{ old('jml_jamaah', $kegiatan->jml_jamaah) }}"
                                                        autocomplete="jml_jamaah" autofocus>
                                                    @error('jml_jamaah')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="deskripsi" class="col-sm-2 col-form-label">Keterangan</label>
                                                <div class="col-sm-10">
                                                    <textarea name="deskripsi" cols="20" rows="5"
                                                        class="form-control @error('deskripsi') is-invalid @enderror"
                                                        name="deskripsi"
                                                        autofocus>{{ old('deskripsi', $kegiatan->deskripsi) }}</textarea>
                                                    @error('deskripsi')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                                <div class="col-sm-10">
                                                    <input id="alamat" type="text"
                                                        class="form-control @error('alamat') is-invalid @enderror"
                                                        name="alamat" value="{{ old('alamat', $kegiatan->alamat) }}"
                                                        autocomplete="alamat" autofocus>
                                                    @error('alamat')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-2 col-form-label">Pilih Kabupaten/Kota</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id='sel_depart' name="id_kab">
                                                        @foreach ($kab as $kabs)
                                                            <option value="{{ $kabs->id }}"
                                                                {{ $kabs->id == $kegiatan->id_kab ? 'selected' : '' }}>
                                                                {{ $kabs->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-2 col-form-label">Pilih Kecamatan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="id_kec" id='sel_emp'>
                                                        @foreach ($kec as $kecs)
                                                            <option value="{{ $kecs->id }}"
                                                                {{ $kecs->id == $kegiatan->id_kec ? 'selected' : '' }}>
                                                                {{ $kecs->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-2 col-form-label">Pilih Kelurahan/Desa</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="id_desa" id='sel_vil'>
                                                        @foreach ($desa as $desas)
                                                            <option value="{{ $desas->id }}"
                                                                {{ $desas->id == $kegiatan->id_desa ? 'selected' : '' }}>
                                                                {{ $desas->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-file row justify-content-end">
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
