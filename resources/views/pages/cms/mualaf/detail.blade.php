@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h3 class="h3 mb-0 text-gray-800">Detail Mualaf</h3>
                                    </div>
                                    <!-- Content Row -->
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Nama Ustadz</th>
                                            <td>{{ $mualaf->user->username }}<b>
                                        </tr>
                                        <tr>
                                            <th>Nomor Handphone</th>
                                            <td>{{ $mualaf->user->nohp }}<b>
                                        </tr>
                                        <tr>
                                            <th>Alamat Ustadz</th>
                                            <td>{{ $mualaf->user->alamat }}<b>
                                        </tr>
                                        <tr>
                                            <th>Tanggal</th>
                                            <td>{{ $mualaf->tgl }}</td>
                                        </tr>
                                        <tr>
                                            <th>Jumlah Mualaf</th>
                                            <td class="text-orange">{{ $mualaf->jml_mualaf }}</td>
                                        </tr>
                                        {{-- <tr>
                                            <th>Alamat</th>
                                            <td class="lower">{{ $mualaf->alamat }}. <br>
                                                {{ $mualaf->desa->name }}. {{ $mualaf->kec->name }}.
                                                {{ $mualaf->kab->name }}</td>
                                        </tr> --}}
                                        <tr>
                                            <th width="25%">Keterangan</th>
                                            <td>{{ $mualaf->deskripsi }}</td>
                                        </tr>
                                        <tr>
                                            <th>Data Mualaf</th>
                                            <td>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Alamat</th>
                                                        <th>Foto KTP</th>
                                                    </tr>
                                                    @php $no = 1; @endphp
                                                    @foreach ($mualaf->fotos as $detail)
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{{ $detail->nama }}</td>
                                                            <td>{{ $detail->alamat }}</td>
                                                            <td>
                                                                <div style="width: 25rem;">
                                                                    <img src="{{ Storage::url('/dokumentasi/' . $detail->foto) }}"
                                                                        class="img-thumbnail"><br>{{ $detail->foto }}
                                                                    <div class="card-body">
                                                                        <h6 class="card-title text-center">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
