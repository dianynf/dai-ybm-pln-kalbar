@extends('layouts.admin')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">Edit Data</h3>
                                        <hr>
                                        <form method="POST" action="{{ route('mualafs.update', $mualaf->id) }}">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-group row">
                                                <label for="tgl" class="col-sm-2 col-form-label">Tanggal</label>
                                                <div class="col-sm-10">
                                                    <input id="tgl" type="text"
                                                        class="form-control datepicker @error('tgl') is-invalid @enderror"
                                                        name="tgl" value="{{ old('tgl', $mualaf->tgl) }}"
                                                        autocomplete="tgl" autofocus>
                                                    @error('tgl')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jml_mualaf" class="col-sm-2 col-form-label">Jumlah
                                                    Mualaf</label>
                                                <div class="col-sm-10">
                                                    <input id="jml_mualaf" type="number" class="form-control"
                                                        name="jml_mualaf"
                                                        value="{{ old('jml_mualaf', $mualaf->jml_mualaf) }}"
                                                        autocomplete="jml_mualaf" autofocus>
                                                    @error('jml_mualaf')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="deskripsi" class="col-sm-2 col-form-label">Keterangan</label>
                                                <div class="col-sm-10">
                                                    <textarea name="deskripsi" cols="20" rows="5"
                                                        class="form-control @error('deskripsi') is-invalid @enderror"
                                                        name="deskripsi"
                                                        autofocus>{{ old('deskripsi', $mualaf->deskripsi) }}</textarea>
                                                    @error('deskripsi')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                                <div class="col-sm-10">
                                                    <input id="alamat" type="text"
                                                        class="form-control @error('alamat') is-invalid @enderror"
                                                        name="alamat" value="{{ old('alamat', $mualaf->alamat) }}"
                                                        autocomplete="alamat" autofocus>
                                                    @error('alamat')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div> --}}
                                            {{-- <div class="row form-group">
                                                <label class="col-sm-2 col-form-label">Pilih Kabupaten/Kota</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id='sel_depart' name="id_kab">
                                                        @foreach ($kab as $kabs)
                                                            <option value="{{ $kabs->id }}"
                                                                {{ $kabs->id == $mualaf->id_kab ? 'selected' : '' }}>
                                                                {{ $kabs->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-2 col-form-label">Pilih Kecamatan</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="id_kec" id='sel_emp'>
                                                        @foreach ($kec as $kecs)
                                                            <option value="{{ $kecs->id }}"
                                                                {{ $kecs->id == $mualaf->id_kec ? 'selected' : '' }}>
                                                                {{ $kecs->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-sm-2 col-form-label">Pilih Kelurahan/Desa</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="id_desa" id='sel_vil'>
                                                        @foreach ($desa as $desas)
                                                            <option value="{{ $desas->id }}"
                                                                {{ $desas->id == $mualaf->id_desa ? 'selected' : '' }}>
                                                                {{ $desas->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div> --}}
                                            <div class="form-file row justify-content-end">
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
