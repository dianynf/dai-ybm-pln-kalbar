@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        <div class="row col-mb-12">
            <!-- Earnings (Monthly) Card Example -->
            @if (Auth::user()->role_id == 1)
                <div class="col-xl-4 col-md-6 mt-2">
                    <div class="card border-left-primary shadow">
                        <div class="card-body">
                            <h6 class="h6 font-weight-bold text-gray-800 text-center">Jumlah Wilayah</h6>
                            <div class="text-center mt-3">
                                <div class="row">
                                    <div class="col-sm">
                                        Kabupaten <br><b>{{ $total_kab }}</b>
                                    </div>
                                    <div class="col-sm">
                                        Kecamatan <br><b>{{ $total_kec }}</b>
                                    </div>
                                    <div class="col-sm">
                                        Desa <br><b>{{ $total_desa }}</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-2 col-md-6 mt-2">
                    <div class="card border-left-info shadow">
                        <div class="card-body">
                            <h6 class="h6 font-weight-bold text-gray-800 text-center">Jumlah Da'i</h6>
                            <div class="text-center mt-4">
                                <h3 class="font-weight-bold">{{ $total_dai }}</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 mt-2">
                    <div class="card border-left-success shadow">
                        <div class="card-body">
                            <h6 class="h6 font-weight-bold text-gray-800 text-center">Jumlah Kegiatan</h6>
                            <div class="text-center mt-4">
                                <h3 class="font-weight-bold">{{ $total_kegiatan }}</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 mt-2">
                    <div class="card border-left-success shadow">
                        <div class="card-body">
                            <h6 class="h6 font-weight-bold text-gray-800 text-center">Jumlah Mualaf</h6>
                            <div class="text-center mt-4">
                                <h3 class="font-weight-bold">{{ $total_mualaf }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif (Auth::user()->role_id == 2)
                <div class="col-xl-4 col-md-6">
                    <div class="card border-left-primary shadow">
                        <div class="card-body">
                            <h6 class="h6 font-weight-bold text-gray-800 text-center">Jumlah Da'i</h6>
                            <div class="text-center mt-4">
                                <h3 class="font-weight-bold">{{ $total_dai }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6">
                    <div class="card border-left-info shadow">
                        <div class="card-body">
                            <h6 class="h6 font-weight-bold text-gray-800 text-center">Jumlah Wilayah</h6>
                            <div class="text-center mt-4">
                                <h3 class="font-weight-bold">12</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6">
                    <div class="card border-left-success shadow">
                        <div class="card-body">
                            <h6 class="h6 font-weight-bold text-gray-800 text-center">Jumlah Mualaf</h6>
                            <div class="text-center mt-4">
                                <h3 class="font-weight-bold">{{ $total_mualaf }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            @else
            @endif
        </div>
        <div class="row mt-4">
            <div class="col-xl-12 col-lg-7">
                <div class="card border-left-primary shadow mb-4">
                    <div class="card-header py-3">
                        <h5 class="m-0 font-weight-bold text-gray-800">Kegiatan Da'i 2021</h5>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="kegiatanChart"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>

            </div>
        </div>
        <div class="row mt-2">
            <div class="col-xl-12 col-lg-7">
                <div class="card border-left-success shadow mb-4">
                    <div class="card-header py-3">
                        <h5 class="m-0 font-weight-bold text-gray-800">Data Mualaf 2021</h5>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="myMualafs"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    @endsection
