@extends('layouts.app')

@section('title')
    Kegiatan Da'i
@endsection

@section('content')
    <main id="main">
        <section id="event-login" class="event-login mt-5">
            <div class="container">
                <div class="card">
                    <div class="col-md-12">
                        <div class="card-body shadow p-3 mb-5 bg-white rounded">
                            @if (session()->has('sukses'))
                                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                    {{ session()->get('sukses') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @if (session()->has('gagal'))
                                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                    {{ session()->get('gagal') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form method="POST" action="/kegiatan/store" enctype="multipart/form-data">
                                @csrf
                                <h2 class="col mb-4 mt-4">Input Kegiatan {{ Auth::user()->username }}
                                </h2>
                                <hr>
                                <div class="col form-group">
                                    <label>Tanggal</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="tgl"
                                            value=" {{ old('tgl') }}" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="icofont-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @error('tgl')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <script type="text/javascript">
                                    $(function() {
                                        $(".datepicker").datepicker({
                                            format: '  dd/mm/yyyy',
                                            autoclose: true,
                                            todayHighlight: true,
                                        });
                                    });

                                </script>

                                <div class="col form-group mt-4">
                                    <label>Jumlah Kajian dalam Satu Pekan</label>
                                    <input type="number" class="form-control" name="jml_kajian"
                                        value="{{ old('jml_kajian') }}" required autofocus>
                                    @error('jml_kajian')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group mt-4">
                                    <label>Jumlah Jam'ah Kajian</label>
                                    <input type="number" class="form-control" name="jml_jamaah"
                                        value="{{ old('jml_jamaah') }}" required autofocus>
                                    @error('jml_jamaah')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group">
                                    <label>Ceritakan Perjalanan Da'wah</label>
                                    <textarea class="form-control" name="deskripsi" required="required" rows="6"
                                        value="{{ old('deskripsi') }}"></textarea>
                                    @error('deskripsi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group">
                                    <label>Foto Kegiatan <span style="color: #ff0000">*</span></label>
                                    <div class="field_wrapper">
                                        <div class="form-group">
                                            <div class="input-group realprocode control-group lst increment mt-2">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="input-group realprocode control-group lst increment mt-2">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="input-group realprocode control-group lst increment mt-2">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="input-group realprocode control-group lst increment mt-2">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="input-group realprocode control-group lst increment mt-2">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span style="color: #d06819;">
                                        Gambar yang Anda upload sebaiknya memiliki
                                        berukuran tidak lebih dari 2MB.
                                    </span>
                                </div>

                                <div class="col form-group mt-4">
                                    <label>Alamat Kegiatan</label>
                                    <input type="text" class="form-control" name="alamat" value="{{ old('alamat') }}"
                                        required autofocus>
                                    @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kabupaten/Kota</label>
                                    <select class="form-control" id='sel_depart' name="id_kab">
                                        <option value="0">Kabupaten/Kota</option>
                                        @foreach ($departmentData['data'] as $department)
                                            <option value='{{ $department->id }}'>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kecamatan</label>
                                    <select class="form-control" name="id_kec" id='sel_emp'>
                                        <option value='0'>Kecamatan</option>
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kelurahan/Desa</label>
                                    <select class="form-control" name="id_desa" id='sel_vil'>
                                        <option value='0'>Desa</option>
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <button type="submit"
                                        class="btn btn-learn-btn col-md-4 offset-md-4 mt-4 mb-3">Kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
