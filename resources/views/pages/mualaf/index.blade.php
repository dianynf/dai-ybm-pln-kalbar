@extends('layouts.app')

@section('title')
    Kegiatan Da'i
@endsection

@section('content')
    <main id="main">
        <section id="event-login" class="event-login mt-5">
            <div class="container">
                <div class="card">
                    <div class="col-md-12">
                        <div class="card-body shadow p-3 mb-5 bg-white rounded">
                            @if (session()->has('sukses'))
                                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                    {{ session()->get('sukses') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form method="POST" action="mualaf/create" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <h3 class="col mb-4 mt-4">Input Data Mualaf dari {{ Auth::user()->username }}
                                </h3>
                                <hr>
                                <div class="col form-group">
                                    <label>Tanggal</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="tgl"
                                            value="{{ old('tgl') }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="icofont-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                    @error('tgl')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <script type="text/javascript">
                                    $(function() {
                                        $(".datepicker").datepicker({
                                            format: '  dd/mm/yyyy',
                                            autoclose: true,
                                            todayHighlight: true,
                                        });
                                    });

                                </script>
                                <div class="col form-group mt-4">
                                    <label>Jumlah Mualaf</label>
                                    <input type="number" class="form-control" name="jml_mualaf"
                                        value="{{ old('jml_mualaf') }}" required autofocus>
                                    @error('jml_mualaf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group mt-4">
                                    <label>Ceritakan Bagaimana Mengenal Islam</label>
                                    <textarea class="form-control" name="deskripsi" required="required" rows="6"
                                        value="{{ old('deskripsi') }}"></textarea>
                                    @error('deskripsi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group mt-4">
                                    <label>Data Mualaf</label>
                                    <div class="form-row">
                                        <div class="col-md-4">
                                            <div class="input-group realprocode control-group lst increment">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-primary" type="button" id="add_button"><i
                                                            class="icofont-plus-square"></i></button>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="text" name="nama" class="form-control" id="nama"
                                                        placeholder="Nama" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <input type="text" required class="form-control" name="alamat" id="alamat"
                                                placeholder="Alamat" data-rule="alamat"
                                                data-msg="Please enter a valid alamat" />
                                            <div class="validate"></div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <div class="form-group">
                                                <div class="input-group realprocode control-group lst increment">
                                                    <div class="custom-file">
                                                        {{-- <input type="file" name="foto[]" id="foto"
                                                            class="myfrm form-control custom-file-input">
                                                        <label class="custom-file-label" for="file01">Upload
                                                            File</label> --}}
                                                        {{-- <input type="text" name="foto[]" id="foto" class="form-control"> --}}
                                                        {{-- <label class="custom-file-label" for="file01">Upload
                                                            File</label> --}}
                                                        <input type="file" name="foto[]" class="form-control-file"
                                                            id="exampleFormControlFile1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="field_wrapper">
                                    </div>

                                    <div class="form-row  realprocode control-group lst increment">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-danger" type="button"><i
                                                                class="icofont-trash fldemo glyphicon glyphicon-remove"></i></button>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="text" name="nama" class="form-control" id="nama"
                                                            placeholder="Nama" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <input type="text" required class="form-control" name="alamat" id="alamat"
                                                placeholder="Alamat" data-rule="alamat"
                                                data-msg="Please enter a valid alamat" />
                                            <div class="validate"></div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        {{-- <input type="text" name="foto[]" id="foto" class="form-control"> --}}
                                                        {{-- <label class="custom-file-label" for="file01">Upload
                                                            File</label> --}}
                                                        <input type="file" name="foto[]" class="form-control-file"
                                                            id="exampleFormControlFile1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col form-group mt-4">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control" name="alamat" value="{{ old('alamat') }}"
                                        required autofocus>
                                    @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kabupaten/Kota</label>
                                    <select class="form-control" id='sel_depart' name="id_kab">
                                        <option value="0">Kabupaten/Kota</option>
                                        @foreach ($departmentData['data'] as $department)
                                            <option value='{{ $department->id }}'>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kecamatan</label>
                                    <select class="form-control" name="id_kec" id='sel_emp'>
                                        <option value='0'>Kecamatan</option>
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kelurahan/Desa</label>
                                    <select class="form-control" name="id_desa" id='sel_vil'>
                                        <option value='0'>Desa</option>
                                    </select>
                                </div> --}}
                                <div class="col form-group">
                                    <button type="submit"
                                        class="btn btn-learn-btn col-md-4 offset-md-4 mt-4 mb-3">Kirim</button>
                                </div>
                            </form>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                var maxField = 10; //Input fields increment limitation
                                var addButton = $('#add_button'); //Add button selector
                                var wrapper = $('.field_wrapper'); //Input field wrapper
                                var fieldHTML =
                                    '<div class="form-row realprocode control-group lst increment"><div class="col-md-4"><div class="field_wrapper"><div class="form-group"><div class="input-group"><div class="input-group-prepend"><button class="btn btn-danger" type="button"><i class="icofont-trash fldemo glyphicon glyphicon-remove"></i></button></div><div class="custom-file"><input type="text" name="nama" class="form-control" id="nama" placeholder="Nama" /></div></div></div></div></div>';
                                fieldHTML = fieldHTML +
                                    '<div class="col-md-4 form-group"><input type="text" required class="form-control" name="alamat" id="alamat" placeholder="Alamat" data-rule="alamat" data-msg="Please enter a valid alamat" /><div class="validate"></div></div>';
                                fieldHTML = fieldHTML +
                                    '<div class="col-md-4 form-group"><div class="form-group"><div class="input-group"><div class="custom-file"><input type="file" name="foto[]" class="form-control-file" id="exampleFormControlFile1"></div></div></div></div>';
                                var x = 1; //Initial field counter is 1

                                //Once add button is clicked
                                $(addButton).click(function() {
                                    //Check maximum number of input fields
                                    if (x < maxField) {
                                        x++; //Increment field counter
                                        $(wrapper).append(fieldHTML); //Add field html
                                    }
                                });

                                //Once remove button is clicked

                                $("body").on("click", ".btn-danger", function() {
                                    $(this).parents(".realprocode").remove();
                                });
                            });

                        </script>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
