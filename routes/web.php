<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')
    ->name('home');

Route::get('/profiles', 'MenuController@profiles')
    ->name('profiles');
Route::get('/berita', 'MenuController@berita')
    ->name('berita');
Route::get('/berita/ustyanto', 'MenuController@ustyanto')
    ->name('detail.ustyanto');
Route::get('/berita/beting-to-bening', 'MenuController@beting_to_bening')
    ->name('detail.beting_to_bening');
Route::get('/berita/training-imam-dan-khatib', 'MenuController@training_imam_dan_khatib')
    ->name('detail.training_imam_dan_khatib');
Route::get('/gambar', 'MenuController@gambar')
    ->name('gambar');
Route::get('/kontak', 'MenuController@kontak')
    ->name('kontak');
Route::post('/kontak/store', 'MenuController@store');
Route::post('/kontak/stores', 'MenuController@stores');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/kegiatan', 'KegiatanController@input')
        ->name('kegiatan_input');;

    Route::post('/kegiatan/store', 'KegiatanController@store');

    Route::resource(
        'kegiatans',
        'KegiatanController',
        ['except' => ['create']]
    );

    Route::get('/mualaf', 'MualafController@input')
        ->name('mualaf');

    Route::post('/mualaf/create', 'MualafController@create');

    Route::resource(
        'mualafs',
        'MualafController',
        ['except' => ['create']]
    );

    Route::get('/getDistricts/{id}', 'KegiatanController@getDistricts');

    Route::get('/getVillages/{id}', 'KegiatanController@getVillages');



    Route::get('/profile', 'ProfileController@edit')
        ->name('profile.edit');

    Route::patch('/profile', 'ProfileController@update')
        ->name('profile.update');

    Route::get('password', 'PasswordController@edit')
        ->name('user.password.edit');

    Route::patch('password', 'PasswordController@update')
        ->name('user.password.update');

    Route::get('dashboard', 'DashboardController@index')
        ->name('dashboard.index');

    Route::get('kegiatan/export-excel/', 'KegiatanController@export_excel');
    Route::get('mualaf/export-excel/', 'MualafController@export_excel');
});

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function () {
    Route::resource('/users', 'UsersController', ['except' => ['show', 'create', 'store']]);
});

// admin
Route::prefix('admin')
    ->namespace('Admin')
    ->middleware(['auth', 'admin'])
    ->group(function () {
        Route::resource('users', 'UsersController');
        Route::resource('pesan', 'PesanController');
        Route::get('dai', 'UsersController@dai')
            ->name('dai.index');
        Route::get('/dai/{user}/ubah', 'UsersController@ubah')->name('dai.ubah');
        Route::put('/dai/simpan/{user}', 'UsersController@simpan')->name('dai.simpan');
        Route::get('/dai/detail/{user}', 'UsersController@detail')->name('dai.detail');
        Route::delete('/dai/delete/{user}', 'UsersController@delete')->name('dai.delete');

        Route::resource('role', 'RoleController');
    });

Auth::routes(['verify' => true]);
